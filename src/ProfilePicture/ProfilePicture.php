<?php

namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database;

class ProfilePicture extends Database
{
    public $id;
    public $name;
    public $profilePicture;


    public function setData($postArray)
    {
        if (array_key_exists("id", $postArray)) {
            $this->id = $postArray['id'];
        }
        if (array_key_exists("name", $postArray)) {
            $this->name = $postArray['name'];
        }
        if (array_key_exists("profilePicture", $postArray)) {
            $this->profilePicture = $postArray['profilePicture'];
        }
    } //end of the setData()

    public function store()
    {

        $name = $this->name;
        $profilePicture = $this->profilePicture;

        // $sqlQuery = "INSERT INTO book_title (book_title, author_name) VALUES('$bookTitle', '$authorName');"; // not secured   

        $sqlQuery = "INSERT INTO `profile_picture` (`name`, `profile_picture`) VALUES(?, ?);"; //secure from the hacker

        $STH = $this->DBH->prepare($sqlQuery);  //STH = statement Handle Prepare method would be used for keeping the data in DB

        $dataArray = array($name, $profilePicture);
        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("Congrats! Data Has been Inserted Successfully! <br>");
        } else {
            Message::message("Error! Data Has not been Inserted! <br>");
        }
    } //end of store() method

}
