<?php

namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database;

class BookTitle extends Database
{
    public $id;
    public $bookTitle;
    public $authorName;

    public function setData($postArray)
    {
        if (array_key_exists("id", $postArray)) {
            $this->id = $postArray['id'];
        }
        if (array_key_exists("bookName", $postArray)) {
            $this->bookTitle = $postArray['bookName'];
        }
        if (array_key_exists("authorName", $postArray)) {
            $this->authorName = $postArray['authorName'];
        }
    } //end of the setData()

    public function store()
    {

        $bookTitle = $this->bookTitle;
        $authorName = $this->authorName;

        // $sqlQuery = "INSERT INTO book_title (book_title, author_name) VALUES('$bookTitle', '$authorName');"; // not secured   

        $sqlQuery = "INSERT INTO book_title (book_title, author_name) VALUES(?, ?);"; //secure from the hacker

        // $result = $this->DBH->exec($sqlQuery);  //Not secured  

        $STH = $this->DBH->prepare($sqlQuery);  //STH = statement Handle Prepare method would be used for keeping the data in DB

        //$this->DBH->query($sqlQuery); //It'll be used for the Data read

        $dataArray = array($bookTitle, $authorName);
        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("Congrats! Data Has been Inserted Successfully! <br>");
        } else {
            Message::message("Error! Data Has not been Inserted! <br>");
        }
    } //end of store() method



}//end of the Title Class
