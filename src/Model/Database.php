<?php

namespace App\Model;

use PDO;
use PDOException;

class Database
{

    public $DBH;

    public function __construct()
    {
        try {
            $this->DBH = new PDO("mysql:host=localhost; dbname=atomic_project", "root", "mysql");
            echo "Database connection successfully!>" . "</br>";
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
}
