<?php

use App\Message\Message;

require_once("../../vendor/autoload.php");
if (!isset($_SESSION)) session_start();
$msg = Message::message();

echo "<div><div id='message'>$msg</div></div>";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Book Title Add Form</title>
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/bootstrap/js/bootstrap.min.js">
    <style>
        body {
            background: antiquewhite;
        }
    </style>
</head>

<body>
    <div class="container">
        <h1 style="color:blue; padding:0px 0px 25px 18px">Book Information Entry</h1>
    </div>

    <div class="container">
        <div class="col-lg-6 offset-lg-3">
            <form action="store.php" method="post">
                <div class="form-group">
                    <strong><label for="">Please Enter Book Name: </label></strong>
                    <input type="text" class="form-control" id="" name="bookName" required>
                </div>
                <div class="form-group">
                    <strong><label for="">Please Enter Author Name: </label></strong>
                    <input type="text" class="form-control" id="" name="authorName" required>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>

    </div>

    <script src="../../resources/bootstrap/js/jquery.js"></script>
    <script>
        jQuery(
            function($) {
                $('#message').fadeOut(550);
                $('#message').fadeIn(550);
                $('#message').fadeOut(550);
                $('#message').fadeIn(550);
                $('#message').fadeOut(550);
                $('#message').fadeIn(550);
                $('#message').fadeOut(550);
            }
        )
    </script>

</body>

</html>