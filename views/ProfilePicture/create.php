<?php

use App\Message\Message;

require_once("../../vendor/autoload.php");
if (!isset($_SESSION)) session_start();
$msg = Message::message();

echo "<div><div id='message'>$msg</div></div>";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile Picture Entry</title>
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/bootstrap/js/bootstrap.min.js">
    <style>
        body {
            background: antiquewhite;
        }
    </style>
</head>

<body>
    <div class="container">
        <h1 style="color:blue; padding:0px 0px 25px 18px">Profile Picture Entry</h1>
    </div>

    <div class="container">
        <div class="col-lg-6 offset-lg-3">
            <form class="form-group f" action="store.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <strong><label for="">Please Enter Person's Name: </label></strong>
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>
                <div class="form-group">
                    <strong><label for="">Enter Person's Profile Picture: </label></strong>
                    <input class="form-control" type="file" id="image" name="image" accept=".png, .jpg, .jpeg" required>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>

    </div>

    <script src="../../resources/bootstrap/js/jquery.js"></script>
    <script>
        jQuery(
            function($) {
                $('#message').fadeOut(550);
                $('#message').fadeIn(550);
                $('#message').fadeOut(550);
                $('#message').fadeIn(550);
                $('#message').fadeOut(550);
                $('#message').fadeIn(550);
                $('#message').fadeOut(550);
            }
        )
    </script>

</body>

</html>